/**
 * @file
 * Statistics functionality.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  $(document).ready(function () {
    $.ajax({
      type: "POST",
      cache: false,
      url: drupalSettings.statistics_ng.url,
      data: drupalSettings.statistics_ng.data
    });
  });
})(jQuery, Drupal, drupalSettings);
