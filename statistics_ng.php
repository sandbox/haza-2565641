<?php

/**
 * @file
 * Handles counts of node views via AJAX with minimal bootstrap.
 */

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

chdir('../../..');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();

$views = $kernel->getContainer()
  ->get('config.factory')
  ->get('statistics_ng.settings')
  ->get('count_content_views');

if ($views) {
  $nid = filter_input(INPUT_POST, 'nid', FILTER_VALIDATE_INT);
  $uid = filter_input(INPUT_POST, 'uid', FILTER_VALIDATE_INT);
  if ($nid) {
    \Drupal::database()->merge('statistics_ng_node_counter')
      ->key('nid', $nid)
      ->fields(array(
        'totalcount' => 1,
        'uid' => $uid,
        'timestamp' => REQUEST_TIME,
      ))
      ->condition('uid', $uid)
      ->expression('totalcount', 'totalcount + 1')
      ->execute();
  }
}

